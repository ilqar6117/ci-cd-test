package az.atl.cicdtest.service;

import az.atl.cicdtest.dto.UserDto;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    public UserDto getUser(){
        return new UserDto("Mamed", 39);
    }

}
