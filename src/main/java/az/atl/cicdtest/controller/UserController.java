package az.atl.cicdtest.controller;

import az.atl.cicdtest.dto.UserDto;
import az.atl.cicdtest.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {

    private final UserService service;

    @GetMapping
    public ResponseEntity<UserDto> getUser(){
        return ResponseEntity.ok(service.getUser());
    }

}
