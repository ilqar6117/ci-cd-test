package az.atl.cicdtest.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = {UserService.class})
class UserServiceTest {

    @Autowired
    UserService service;

    @Test
    void getUser() {
        Assertions.assertDoesNotThrow(()->service.getUser());
    }

}