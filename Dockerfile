FROM openjdk:11

COPY build/libs/ci-cd-test-0.0.1-SNAPSHOT.jar app.jar

EXPOSE 8080

CMD ["java", "-jara", "app.jar"]

